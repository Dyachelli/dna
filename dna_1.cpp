#include "dna_1.h"
#include "ui_dna_1.h"
#include "two_string.h"
#include "for_int.h"

/*
Problem #1

A string is simply an ordered collection of symbols selected from some alphabet and formed into a word;
the length of a string is the number of symbols that it contains.

An example of a length 21 DNA string (whose alphabet contains the symbols 'A', 'C', 'G', and 'T')
is "ATGCTTCAGAAAGGTCTTACG."

Given: A DNA string s of length at most 1000 nt.

Return: Four integers (separated by spaces) counting the respective number of
times that the symbols 'A', 'C', 'G', and 'T' occur in s.
*/

dna_1::dna_1(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::dna_1)
{
   ui->setupUi(this);
}

dna_1::~dna_1()
{
   delete ui;
}

void dna_1::on_pushButton_clicked()
{
   if(!ui->reverse->isChecked()&&!ui->protein->isChecked())load_1();
   else if(ui->reverse->isChecked())load_2();

}

void dna_1::load_1(){
   QString dna = ui->textEdit->toPlainText();
   int a = 0, c = 0, g = 0, t = 0;
   for(int i = 0; i < dna.length(); i++){
      if(dna[i] == 'A') a++;
         else if(dna[i] == 'C') c++;
            else if(dna[i] == 'G') g++;
               else if(dna[i] == 'T') t++;
   }
   ui->lineEdit->setText("A:"+QString::number(a)+" C:"+QString::number(c)+" G:"+QString::number(g)+" T:"+QString::number(t));
   load_3();
}

void dna_1::load_2(){
   QString dna = ui->textEdit->toPlainText();
   QString dna1 = "", dna2 = "",dna_reverse = "",buf1 = "", buf2 = "";
   int half_dna = dna.length()/2, j = 0;

   for(int i = 0; i < dna.length(); i++)
      if(i<half_dna)dna1[i] = dna[i];
      else dna2[i-half_dna] = dna[i];
   buf1 = dna1;
   buf2 = dna2;
  //перевернуть обе половины днк
   for(int i = half_dna-1; i >= 0; i-- ){
      dna2[j] = buf2[i];
      dna1[j] = buf1[i];
      j++;
   }
   for(int i = 0; i < dna.length(); i++){
      if(i < half_dna){
         if(dna2[i] == 'A')dna_reverse[i] = 'T';
            else if(dna2[i] == 'C')dna_reverse[i] = 'G';
               else if(dna2[i] == 'G')dna_reverse[i] = 'C';
                  else if(dna2[i] == 'T')dna_reverse[i] = 'A';
      }else{
         if(dna1[i-half_dna] == 'A')dna_reverse[i] = 'T';
            else if(dna1[i-half_dna] == 'C')dna_reverse[i] = 'G';
               else if(dna1[i-half_dna] == 'G')dna_reverse[i] = 'C';
                  else if(dna1[i-half_dna] == 'T')dna_reverse[i] = 'A';

      }
   }
   ui->textEdit_2->setText(dna_reverse);

}

void dna_1::load_3(){
   QString dna = ui->textEdit->toPlainText(), rna = "";
   for(int i = 0; i < dna.length(); i++){
      if(dna[i] != 'T'&&dna[i] != 't')rna[i] = dna[i];
      else rna[i] = 'U';
   }
   ui->textEdit_2->setText(rna);
}

void dna_1::on_reverse_clicked()
{
   if(ui->reverse->isChecked()) ui->label_2->setText("REVERSE");
   else ui->label_2->setText("RNA");
}

void dna_1::on_actionCounting_Point_Mutations_triggered()
{
    two_string s;
    s.type = 1;
    s.setModal(true);
    s.exec();
}

void dna_1::on_actionFinding_a_Motif_in_DNA_triggered()
{
    two_string s;
    s.type = 2;
    s.setModal(true);
    s.exec();
}

void dna_1::on_actionTranslating_RNA_into_Protein_triggered()
{
   two_string s;
   s.type = 3;
   s.setModal(true);
   s.exec();
}

void dna_1::on_actionRabbits_and_Recurrence_Relations_triggered()
{
   for_int s;
   s.type = 1;
   s.setModal(true);
   s.exec();

}
