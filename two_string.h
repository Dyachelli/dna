#ifndef TWO_STRING_H
#define TWO_STRING_H

#include <QDialog>

namespace Ui {
class two_string;
}

class two_string : public QDialog
{
    Q_OBJECT

public:
    unsigned type;
    explicit two_string(QWidget *parent = 0);
    ~two_string();

    int load_1(QString dna1, QString dna2);
    QString load_2(QString dna, QString motif);
    bool find_motif(QString dna, QString motif,int point);
    void load_3(QString rna);
    char translate_codon(QString codon);

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();


private:
    Ui::two_string *ui;
};

#endif // TWO_STRING_H
