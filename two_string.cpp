#include "two_string.h"
#include "ui_two_string.h"

two_string::two_string(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::two_string)
{
    ui->setupUi(this);
}

two_string::~two_string()
{
    delete ui;
}

void two_string::on_pushButton_clicked()
{
    QString dna1 = ui->textEdit->toPlainText(),dna2 = ui->textEdit_2->toPlainText();
    int res = 0;
    QString s_res = "";

    if(type == 1)res = load_1(dna1,dna2);
    else if(type == 2)s_res = load_2(dna1,dna2);
    else if(type == 3)load_3(dna1);

    if(res != 0)ui->lineEdit->setText(QString::number(res));
    else ui->lineEdit->setText(s_res);
}
int two_string::load_1(QString dna1,QString dna2)
{
    int count = 0;
    for(int i = 0; i < dna1.length(); i++){
        if(dna1[i] != dna2[i])count++;
    }
    return count;
}

QString two_string::load_2(QString dna,QString motif){
    QString val = "";
    for(int i = 0; i < dna.length(); i++){
        if(dna[i] == motif[0] && find_motif(dna,motif,i)){
            if(val!= "")val+= " ";
            val+=QString::number(i+1);
        }
    }
    return val;
}
bool two_string::find_motif(QString dna, QString motif, int point){
    bool val = true;
    for(int i = point; i < point+motif.length(); i++)
        if(dna[i]!= motif[i-point])
            val = false;
    return val;
}

void two_string::load_3(QString rna){
   QString protein = "", codon = "";
   int cou = 0;
   bool start = false;
   int i = 0;
   while ( i < rna.length()){
      codon[cou] = rna[i];
      if(cou < 3)cou++;

      if(cou == 3){
         if(codon == QString("AUG")){
            start = true;
            protein += 'M';
         }
         else if(codon == QString("UAA")||codon == QString("UAG") || codon == QString("UGA"))
            start = false;
         else if(start)protein+= translate_codon(codon);
         codon = "";
         cou = 0;
      }
      i++;
   }
   ui->textEdit_2->setText(protein);
}
char two_string::translate_codon(QString codon){
char P;
   if (codon == QString("UGG"))P = 'W';
   else if
      (codon == QString("UAC")
     ||codon == QString("UAU"))P = 'Y';
   else if
      (codon == QString("UGC")
     ||codon == QString("UGU"))P = 'C';
   else if
      (codon == QString("GAA")
     ||codon == QString("GAG"))P = 'E';
   else if
      (codon == QString("AAA")
     ||codon == QString("AAG"))P = 'K';
   else if
      (codon == QString("CAA")
     ||codon == QString("CAG"))P = 'Q';
   else if
      (codon == QString("AGC")
     ||codon == QString("AGU")
     ||codon == QString("UCA")
     ||codon == QString("UCC")
     ||codon == QString("UCG")
     ||codon == QString("UCU"))P = 'S';
   else if
      (codon == QString("UUA")
     ||codon == QString("UUG")
     ||codon == QString("CUA")
     ||codon == QString("CUC")
     ||codon == QString("CUG")
     ||codon == QString("CUU"))P = 'L';
   else if
      (codon == QString("AGA")
     ||codon == QString("AGG")
     ||codon == QString("CGA")
     ||codon == QString("CGC")
     ||codon == QString("CGG")
     ||codon == QString("CGU"))P = 'R';
   else if
      (codon == QString("GGA")
     ||codon == QString("GGC")
     ||codon == QString("GGG")
     ||codon == QString("GGU"))P = 'G';
   else if
      (codon == QString("UUC")
     ||codon == QString("UUU"))P = 'F';
   else if
      (codon == QString("GAC")
     ||codon == QString("GAU"))P = 'D';
   else if
      (codon == QString("CAC")
     ||codon == QString("CAU"))P = 'H';
   else if
      (codon == QString("AAC")
     ||codon == QString("AAU"))P = 'N';
   else if
      (codon == QString("GCA")
     ||codon == QString("GCC")
     ||codon == QString("GCG")
     ||codon == QString("GCU"))P = 'A';
   else if
      (codon == QString("CCA")
     ||codon == QString("CCC")
     ||codon == QString("CCG")
     ||codon == QString("CCU"))P = 'P';
   else if
      (codon == QString("ACA")
     ||codon == QString("ACC")
     ||codon == QString("ACG")
     ||codon == QString("ACU"))P = 'T';
   else if
      (codon == QString("GUA")
     ||codon == QString("GUC")
     ||codon == QString("GUG")
     ||codon == QString("GUU"))P = 'V';
   else if
      (codon == QString("AUA")
     ||codon == QString("AUC")
     ||codon == QString("AUU"))P = 'I';
   return P;
}


void two_string::on_pushButton_2_clicked()
{
    ui->textEdit->setText("");
    ui->textEdit_2->setText("");
    ui->lineEdit->setText("");
}
