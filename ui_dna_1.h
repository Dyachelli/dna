/********************************************************************************
** Form generated from reading UI file 'dna_1.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DNA_1_H
#define UI_DNA_1_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_dna_1
{
public:
    QAction *actionCounting_Point_Mutations;
    QAction *actionFinding_a_Motif_in_DNA;
    QAction *actionTranslating_RNA_into_Protein;
    QAction *actionRabbits_and_Recurrence_Relations;
    QWidget *centralWidget;
    QTextEdit *textEdit;
    QPushButton *pushButton;
    QLineEdit *lineEdit;
    QTextEdit *textEdit_2;
    QLabel *label;
    QLabel *label_2;
    QCheckBox *reverse;
    QCheckBox *protein;
    QMenuBar *menuBar;
    QMenu *menuProblems;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *dna_1)
    {
        if (dna_1->objectName().isEmpty())
            dna_1->setObjectName(QStringLiteral("dna_1"));
        dna_1->resize(450, 382);
        actionCounting_Point_Mutations = new QAction(dna_1);
        actionCounting_Point_Mutations->setObjectName(QStringLiteral("actionCounting_Point_Mutations"));
        actionFinding_a_Motif_in_DNA = new QAction(dna_1);
        actionFinding_a_Motif_in_DNA->setObjectName(QStringLiteral("actionFinding_a_Motif_in_DNA"));
        actionTranslating_RNA_into_Protein = new QAction(dna_1);
        actionTranslating_RNA_into_Protein->setObjectName(QStringLiteral("actionTranslating_RNA_into_Protein"));
        actionRabbits_and_Recurrence_Relations = new QAction(dna_1);
        actionRabbits_and_Recurrence_Relations->setObjectName(QStringLiteral("actionRabbits_and_Recurrence_Relations"));
        centralWidget = new QWidget(dna_1);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(10, 30, 431, 121));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(10, 160, 75, 23));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(90, 160, 113, 20));
        textEdit_2 = new QTextEdit(centralWidget);
        textEdit_2->setObjectName(QStringLiteral("textEdit_2"));
        textEdit_2->setGeometry(QRect(10, 210, 431, 121));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 41, 16));
        QFont font;
        font.setFamily(QStringLiteral("Calibri"));
        font.setPointSize(14);
        label->setFont(font);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 190, 111, 16));
        label_2->setFont(font);
        reverse = new QCheckBox(centralWidget);
        reverse->setObjectName(QStringLiteral("reverse"));
        reverse->setGeometry(QRect(370, 160, 70, 17));
        protein = new QCheckBox(centralWidget);
        protein->setObjectName(QStringLiteral("protein"));
        protein->setGeometry(QRect(370, 180, 70, 17));
        dna_1->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(dna_1);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 450, 21));
        menuProblems = new QMenu(menuBar);
        menuProblems->setObjectName(QStringLiteral("menuProblems"));
        dna_1->setMenuBar(menuBar);
        mainToolBar = new QToolBar(dna_1);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        dna_1->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(dna_1);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        dna_1->setStatusBar(statusBar);

        menuBar->addAction(menuProblems->menuAction());
        menuProblems->addAction(actionCounting_Point_Mutations);
        menuProblems->addAction(actionFinding_a_Motif_in_DNA);
        menuProblems->addAction(actionTranslating_RNA_into_Protein);
        menuProblems->addAction(actionRabbits_and_Recurrence_Relations);

        retranslateUi(dna_1);

        QMetaObject::connectSlotsByName(dna_1);
    } // setupUi

    void retranslateUi(QMainWindow *dna_1)
    {
        dna_1->setWindowTitle(QApplication::translate("dna_1", "dna_1", 0));
        actionCounting_Point_Mutations->setText(QApplication::translate("dna_1", "Counting Point Mutations", 0));
        actionFinding_a_Motif_in_DNA->setText(QApplication::translate("dna_1", "Finding a Motif in DNA", 0));
        actionTranslating_RNA_into_Protein->setText(QApplication::translate("dna_1", "Translating RNA into Protein", 0));
        actionRabbits_and_Recurrence_Relations->setText(QApplication::translate("dna_1", "Rabbits and Recurrence Relations", 0));
        pushButton->setText(QApplication::translate("dna_1", "run", 0));
        label->setText(QApplication::translate("dna_1", "DNA", 0));
        label_2->setText(QApplication::translate("dna_1", "RNA", 0));
        reverse->setText(QApplication::translate("dna_1", "reserve", 0));
        protein->setText(QApplication::translate("dna_1", "protein", 0));
        menuProblems->setTitle(QApplication::translate("dna_1", "Problems", 0));
    } // retranslateUi

};

namespace Ui {
    class dna_1: public Ui_dna_1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DNA_1_H
