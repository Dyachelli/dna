#ifndef FOR_INT_H
#define FOR_INT_H

#include <QDialog>

namespace Ui {
class for_int;
}

class for_int : public QDialog
{
   Q_OBJECT

public:
   explicit for_int(QWidget *parent = 0);
   ~for_int();
   int type;


private slots:
   void on_pushButton_clicked();
   void load_1();

   void on_pushButton_2_clicked();

private:
   Ui::for_int *ui;
};

#endif // FOR_INT_H
