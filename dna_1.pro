#-------------------------------------------------
#
# Project created by QtCreator 2015-07-10T09:31:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dna_1
TEMPLATE = app


SOURCES += main.cpp\
        dna_1.cpp \
    two_string.cpp \
    for_int.cpp

HEADERS  += dna_1.h \
    two_string.h \
    for_int.h

FORMS    += dna_1.ui \
    two_string.ui \
    for_int.ui
