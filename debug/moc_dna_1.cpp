/****************************************************************************
** Meta object code from reading C++ file 'dna_1.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../dna_1.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dna_1.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_dna_1_t {
    QByteArrayData data[11];
    char stringdata[255];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_dna_1_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_dna_1_t qt_meta_stringdata_dna_1 = {
    {
QT_MOC_LITERAL(0, 0, 5), // "dna_1"
QT_MOC_LITERAL(1, 6, 21), // "on_pushButton_clicked"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 18), // "on_reverse_clicked"
QT_MOC_LITERAL(4, 48, 43), // "on_actionCounting_Point_Mutat..."
QT_MOC_LITERAL(5, 92, 41), // "on_actionFinding_a_Motif_in_D..."
QT_MOC_LITERAL(6, 134, 6), // "load_1"
QT_MOC_LITERAL(7, 141, 6), // "load_2"
QT_MOC_LITERAL(8, 148, 6), // "load_3"
QT_MOC_LITERAL(9, 155, 47), // "on_actionTranslating_RNA_into..."
QT_MOC_LITERAL(10, 203, 51) // "on_actionRabbits_and_Recurren..."

    },
    "dna_1\0on_pushButton_clicked\0\0"
    "on_reverse_clicked\0"
    "on_actionCounting_Point_Mutations_triggered\0"
    "on_actionFinding_a_Motif_in_DNA_triggered\0"
    "load_1\0load_2\0load_3\0"
    "on_actionTranslating_RNA_into_Protein_triggered\0"
    "on_actionRabbits_and_Recurrence_Relations_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_dna_1[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x08 /* Private */,
       3,    0,   60,    2, 0x08 /* Private */,
       4,    0,   61,    2, 0x08 /* Private */,
       5,    0,   62,    2, 0x08 /* Private */,
       6,    0,   63,    2, 0x08 /* Private */,
       7,    0,   64,    2, 0x08 /* Private */,
       8,    0,   65,    2, 0x08 /* Private */,
       9,    0,   66,    2, 0x08 /* Private */,
      10,    0,   67,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void dna_1::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        dna_1 *_t = static_cast<dna_1 *>(_o);
        switch (_id) {
        case 0: _t->on_pushButton_clicked(); break;
        case 1: _t->on_reverse_clicked(); break;
        case 2: _t->on_actionCounting_Point_Mutations_triggered(); break;
        case 3: _t->on_actionFinding_a_Motif_in_DNA_triggered(); break;
        case 4: _t->load_1(); break;
        case 5: _t->load_2(); break;
        case 6: _t->load_3(); break;
        case 7: _t->on_actionTranslating_RNA_into_Protein_triggered(); break;
        case 8: _t->on_actionRabbits_and_Recurrence_Relations_triggered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject dna_1::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_dna_1.data,
      qt_meta_data_dna_1,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *dna_1::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *dna_1::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_dna_1.stringdata))
        return static_cast<void*>(const_cast< dna_1*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int dna_1::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
