#ifndef DNA_1_H
#define DNA_1_H

#include <QMainWindow>

namespace Ui {
class dna_1;
}

class dna_1 : public QMainWindow
{
   Q_OBJECT

public:
   explicit dna_1(QWidget *parent = 0);
   ~dna_1();

private slots:
   void on_pushButton_clicked();
   void on_reverse_clicked();
   void on_actionCounting_Point_Mutations_triggered();
   void on_actionFinding_a_Motif_in_DNA_triggered();
   void load_1();
   void load_2();
   void load_3();

   void on_actionTranslating_RNA_into_Protein_triggered();

   void on_actionRabbits_and_Recurrence_Relations_triggered();

private:
   Ui::dna_1 *ui;
};

#endif // DNA_1_H
