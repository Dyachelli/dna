#include "for_int.h"
#include "ui_for_int.h"

for_int::for_int(QWidget *parent) :
   QDialog(parent),
   ui(new Ui::for_int)
{
   ui->setupUi(this);
}

for_int::~for_int()
{
   delete ui;
}

void for_int::on_pushButton_clicked()
{
   if(type == 1)load_1();
}
void for_int::load_1(){
   int num_month = ui->lineEdit->text().toInt();
   int num_little = ui->lineEdit_2->text().toInt();
   unsigned int *pair = new unsigned int[num_month];
   unsigned int *little = new unsigned int[num_month];

   if(pair != NULL){
      pair[0] = 1;
      little[0] = 0;

      for(int i = 1; i < num_month; i++){

         little[i] = pair[i-1]*num_little;
         pair[i] = pair[i-1] + little[i-1];
      }
   }
   ui->res->setText(QString::number(pair[num_month-1]));
   delete []pair;
   delete []little;

}

void for_int::on_pushButton_2_clicked()
{
    ui->lineEdit->clear();;
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    ui->res->clear();
}
